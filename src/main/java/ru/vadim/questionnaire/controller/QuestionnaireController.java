package ru.vadim.questionnaire.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vadim.questionnaire.entity.Questionnaire;
import ru.vadim.questionnaire.entity.dto.QuestionnaireDto;
import ru.vadim.questionnaire.exception.NoEntityException;
import ru.vadim.questionnaire.mapper.QuestionnaireMapper;
import ru.vadim.questionnaire.service.QuestionnaireService;

import java.util.List;

@RestController
@RequestMapping(value = "api/questionnaire/")
@RequiredArgsConstructor
public class QuestionnaireController {
    private final QuestionnaireService service;

    @PostMapping(value = "")
    public ResponseEntity<?> createQuestionnaire(@RequestBody QuestionnaireDto questionnaire) {
        service.save(questionnaire);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping(value = "{id}")
    public ResponseEntity<QuestionnaireDto> getQuestionnaire(@PathVariable(name = "id") Integer id) throws NoEntityException {
       QuestionnaireDto questionnaire = service.findByI(id);

        return questionnaire != null
                ? new ResponseEntity<>(questionnaire, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "active")
    public List<QuestionnaireDto> getActiveQuestionnaire() throws NoEntityException {
        List<QuestionnaireDto> questionnaire = service.findActiveQuestionnaires();
        return questionnaire;
    }

    @DeleteMapping(value = "{id}")
    @Operation(description = "Удаление прошло успешно", responses = {@ApiResponse(responseCode = "200", description = "Удаление опросника")})
    public void deleteQuestionnaire(@PathVariable(name = "id") Integer id) {
        service.delete(id);
    }

    @PutMapping(value = "")
    public void updateProf(@RequestBody QuestionnaireDto questionnaire) {
        service.save(questionnaire);
    }
}
