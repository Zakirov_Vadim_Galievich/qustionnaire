package ru.vadim.questionnaire.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vadim.questionnaire.entity.Question;
import ru.vadim.questionnaire.entity.dto.QuestionDto;
import ru.vadim.questionnaire.exception.NoEntityException;
import ru.vadim.questionnaire.mapper.QuestionMapper;
import ru.vadim.questionnaire.service.QuestionService;

@RestController
@RequestMapping(value = "api/question/")
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionService service;

    @PostMapping(value = "")
    public ResponseEntity<?> createQuote(@RequestBody QuestionDto question) {
        service.save(question);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<QuestionDto> getQuote(@PathVariable(name = "id") Integer id) throws NoEntityException {
        QuestionDto question = service.findByI(id);

        return question != null
                ? new ResponseEntity<>(question, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "{id}")
    @Operation(description = "Удаление прошло успешно", responses = {@ApiResponse(responseCode = "200", description = "Удаление вопроса")})
    public void deleteQuestionnaire(@PathVariable(name = "id") Integer id) {
        service.delete(id);
    }

    @PutMapping(value = "")
    public void updateProf(@RequestBody QuestionDto question) {
        service.save(question);
    }
}
