package ru.vadim.questionnaire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.vadim.questionnaire.entity.dto.QuestionnaireDto;
import ru.vadim.questionnaire.exception.NoEntityException;
import ru.vadim.questionnaire.mapper.QuestionnaireMapper;
import ru.vadim.questionnaire.repository.QustionnaireRepo;

import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionnaireService {
    private final QustionnaireRepo repo;
    private final QuestionnaireMapper mapper;

    public void save(QuestionnaireDto questionnaire) {
        repo.save(mapper.map(questionnaire));
    }

    public QuestionnaireDto findByI(Integer id) throws NoEntityException {
        return mapper.map(repo.findById(id).orElseThrow(NoEntityException::new));
    }

    public List<QuestionnaireDto> findActiveQuestionnaires() {
        return mapper.map(repo.getQuestionnairesByActiveTrue());
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
