package ru.vadim.questionnaire.mapper;

import org.mapstruct.Mapper;
import ru.vadim.questionnaire.entity.Question;
import ru.vadim.questionnaire.entity.Questionnaire;
import ru.vadim.questionnaire.entity.dto.QuestionDto;
import ru.vadim.questionnaire.entity.dto.QuestionnaireDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionMapper {
    List<QuestionDto> map(List<Question> course);

    QuestionDto map(Question course);

    Question map(QuestionDto dto);
}
