package ru.vadim.questionnaire.entity.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.vadim.questionnaire.entity.Question;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class QuestionnaireDto {
    private String name;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private boolean isActive;
    private List<QuestionDto> questions;
}
