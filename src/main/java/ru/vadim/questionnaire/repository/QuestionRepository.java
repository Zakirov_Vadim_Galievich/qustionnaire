package ru.vadim.questionnaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vadim.questionnaire.entity.Question;
@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer> {
}
