package ru.vadim.questionnaire.entity.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.vadim.questionnaire.entity.Questionnaire;
import ru.vadim.questionnaire.entity.Types;

import javax.persistence.*;
@Data
@NoArgsConstructor
public class QuestionDto {
    private String text;
    private Types types;
}
