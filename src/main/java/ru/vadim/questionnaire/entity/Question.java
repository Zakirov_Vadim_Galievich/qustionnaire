package ru.vadim.questionnaire.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.Nullable;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    private String text;

    @Enumerated(EnumType.STRING)
    @Column(name = "types")
    private Types types;

    @ManyToOne
    @JsonBackReference
    @Nullable
    @JoinTable(name = "questionnaire_question", joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn (name = "questionnaire_id"))
    private Questionnaire questionnaire;
}
