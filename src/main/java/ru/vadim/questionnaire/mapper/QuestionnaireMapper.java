package ru.vadim.questionnaire.mapper;

import org.mapstruct.Mapper;
import ru.vadim.questionnaire.entity.Questionnaire;
import ru.vadim.questionnaire.entity.dto.QuestionnaireDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionnaireMapper {
    List<QuestionnaireDto> map(List<Questionnaire> course);

    QuestionnaireDto map(Questionnaire course);

    Questionnaire map(QuestionnaireDto dto);
}
