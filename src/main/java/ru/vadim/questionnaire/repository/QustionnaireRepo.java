package ru.vadim.questionnaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vadim.questionnaire.entity.Questionnaire;
import ru.vadim.questionnaire.entity.dto.QuestionnaireDto;

import java.util.List;

@Repository
public interface QustionnaireRepo extends JpaRepository<Questionnaire, Integer> {
    List<Questionnaire> getQuestionnairesByActiveTrue();
}
