package ru.vadim.questionnaire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.vadim.questionnaire.entity.dto.QuestionDto;
import ru.vadim.questionnaire.exception.NoEntityException;
import ru.vadim.questionnaire.mapper.QuestionMapper;
import ru.vadim.questionnaire.repository.QuestionRepository;

@Service
@RequiredArgsConstructor
public class QuestionService {
    private final QuestionRepository repository;
    private final QuestionMapper mapper;

    public void save(QuestionDto question) {
        repository.save(mapper.map(question));
    }

    public QuestionDto findByI(Integer id) throws NoEntityException {
        return mapper.map(repository.findById(id).orElseThrow(NoEntityException::new));
    }

    public void delete(Integer id) {
        repository.deleteById(id);
    }


}
